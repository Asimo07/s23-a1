// insertOne Method

db.users.insertOne({
        "name": "Single",
        "accomodates": 2,
        "price": 1000,
        "description": "A simple room with all the basic necessities",
        "roomsAvailable": 10,
        "isAvailable": false 
    });

// insertMany Method
db.users.insertMany([
    {
        "name": "Double",
        "accomodates": 3,
        "price": 2000,
        "description": "A room fit for a small family going on a vacation",
        "roomsAvailable": 5,
        "isAvailable": false 
    },
    {
        "name": "Queen",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a simple getaway",
        "roomsAvailable": 15,
        "isAvailable": false 
    }
]);

// find Method searching for room with name double
db.users.find({"name": "Double"});

// use updateOne Method to update queen room to 0

db.users.updateOne(
    {"name": "Queen"},
    { 
        $set: {"roomsAvailable": 0}
    }
);

// use the deleteMany method to delete rooms that have 0 availability
db.users.deleteMany(
    {"roomsAvailable": 0}
);